
var divGenCalendar;

function crearAgenda(divCalendar, fechaHoraActual, divDialog, divDialogMsg, divDialogDelete, divDeleteMsg){
    	       
    divGenCalendar = divCalendar;
    
    $('#'+divCalendar).fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
                                //right: 'month,basicWeek,basicDay'
			},
			defaultDate: $.now(),
                        defaultView: 'basicDay', // month,basicWeek,basicDay,agendaWeek,agendaDay
                        weekends: true, // mostrar fines de semana
                        hiddenDays: [ 0 ], // esconder días
                        //fixedWeekCount: false, // muestra el número de semanas que se pueden visualizar
                        //weekNumbers: true, // mostrar los números de las semanas
                        //handleWindowResize: true, // redimensionar automáticamente
                        timezone: 'America/Guayaquil',
                        lang: 'es',
                        selectable: true,
			selectHelper: true,
			editable: false,//habilita y desabilita la edición en los eventos
			eventLimit: true, // allow "more" link when too many events
                        //disableResizing: false,
                        allDaySlot: false, // mostrar la pestaña todos los días
                        slotLabelFormat: 'H(:mm)a', //
                        slotDuration: '00:15:00', //tiempo de duracción de cada slot
                        slotEventOverlap: false, //indica si se puede o no sobresolapar los tiempos 
                        minTime: '08:00:00',
                        maxTime: '20:00:00',
                        nowIndicator: true,
                   
                        eventClick: function(calEvent, jsEvent, view) {   
                            
                            $("#"+divDialogDelete).dialog({
                                  modal: true,
                                  buttons: {
                                    Eliminar: function() {                                          
                                      removerEvent(calEvent.id);
                                      $( this ).dialog('close');
                                    },

                                    Cancelar: function() {
                                      $( this ).dialog('close');
                                    }
                                }
                            });
                                
                            $('#'+divDeleteMsg).html('Paciente: '+calEvent.title + '<br>Fecha de cita: ' + calEvent.start.format() + '<br> Id: ' + calEvent.id);
                            $(this).css('border-color', 'red');
                            
                            //$('#'+divCalendar).fullCalendar('removeEvents', 7);
                            
                        },
                        dayRender: function(date, cell){                            
                            /*if (date.format() > '2016-04-16'){
                                //$(cell).addClass('disabled');
                            }*/
                        },
                        select: function(start, end) {
                            
                        if (start.format() > fechaHoraActual.format()){
                                                                    
                            $("#"+divDialog).dialog({
                                  modal: true,
                                  buttons: {
                                    'Reservar cita': function() {
                                      crearEvento(generateUniqueID(), $('#selectNombre').val(), start, end, 'green');
                                      $( this ).dialog('close');
                                    },
                                    Cancelar: function() {
                                      $( this ).dialog('close');
                                    }
                                }
                            });
                            
                            $("#"+divDialogMsg).html('<br>Hora de la reserva <hr> Inicio cita: ' + start.format() + ' <br>Fin cita: ' + end.format());
                                        
                            $('#dateStart').val(start);
                            $('#dateEnd').val(end);
                                    
                                }else{
                                    alert('No se puede dar turno para esa fecha seleccionada.');
                                }
			},
			
		});		
            }

            function crearEvento(idPaciente, nombrePaciente, inicio, fin, color){
                var makeEvent = {
                  id: idPaciente,
                  title: nombrePaciente,
                  start: inicio,
                  end: fin,
                  color: color
                };

                $('#'+divGenCalendar).fullCalendar( 'renderEvent', makeEvent, true );
            }

            function removerEvent(id){
                                
                    $('#'+divGenCalendar).fullCalendar('removeEvents', id);
                
               // $('#'+divGenCalendar).fullCalendar( 'renderEvent', makeEvent, true );
            };
            
            function generateUniqueID(){
                return Math.random().toString(10).slice(2);
            }