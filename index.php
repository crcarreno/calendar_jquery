<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />

<link href="css/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="css/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print"/>
<script src="js/moment.min.js" type="text/javascript"></script>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/fullcalendar.min.js" type="text/javascript"></script>
<script src="js/lang-all.js" type="text/javascript"></script>
<script>
alert('on');
	$(document).ready(function(){
		                
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
                                //right: 'month,basicWeek,basicDay'
			},
			defaultDate: $.now(),
                        lang: 'es',
                        selectable: true,
			selectHelper: true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
                        
                        //Modal Event Title
                        select: function(start, end) {
				var title = prompt('Agregue paciente a la agenda');
				var eventData;
				if (title) {
					eventData = {
						title: title,
						start: start,
						end: end
					};
                                        
			$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                                        
				}
                                
				//$('#calendar').fullCalendar('unselect');
                                
			},
			/*events: [
				{
					title: 'All Day Event',
					start: '2016-04-01'
				},
				{
					title: 'Long Event',
					start: '2016-04-07',
					end: '2016-04-10'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2016-04-09T16:00:00'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2016-04-16T16:00:00'
				},
				{
					title: 'Conference',
					start: '2016-01-11',
					end: '2016-01-13'
				},
				{
					title: 'Meeting',
					start: '2016-01-12T10:30:00',
					end: '2016-01-12T12:30:00'
				},
				{
					title: 'Lunch',
					start: '2016-01-12T12:00:00'
				},
				{
					title: 'Meeting',
					start: '2016-01-12T14:30:00'
				},
				{
					title: 'Happy Hour',
					start: '2016-01-12T17:30:00'
				},
				{
					title: 'Dinner',
					start: '2016-01-12T20:00:00'
				},
				{
					title: 'Birthday Party',
					start: '2016-01-13T07:00:00'
				},
				{
					title: 'Click for Google',
					url: 'http://google.com/',
					start: '2016-01-28'
				}
			]*/
		});
		
	});

var myEvent = {
  title:"Paciente: ronald rigan",
  allDay: true,
  start: '2016-04-28',
  end: '2016-04-28'
};

$('#calendar').fullCalendar( 'renderEvent', myEvent, true );

</script>
<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>
</head>
<body>

	<div id='calendar'></div>

</body>
</html>
