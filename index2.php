<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />

<link href="css/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="css/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script src="js/moment.min.js" type="text/javascript"></script>
<script src="js/fullcalendar.min.js" type="text/javascript"></script>
<script src="js/lang-all.js" type="text/javascript"></script>
<script src="agenda.js" type="text/javascript"></script>

<style>
	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	#calendar {
		max-width: 700px;
                left: auto;
		/*margin: 0 auto;*/
	}
</style>
</head>
<body>

    <div style="float: left; margin-right: 40px;"><div id='calendar'></div></div>
        <script>                                    
            crearAgenda('calendar', moment(), 'dialog-cita', 'divDialogMessage', 'dialog-delete', 'divDeleteMsg');

            crearEvento('1', 'Cristhian Carreño', '2016-04-08T17:00:00', '2016-04-08T17:30:00', 'green');
            crearEvento('2', 'Rosa Melano', '2016-04-08T17:30:00', '2016-04-08T17:45:00', 'orange');
            crearEvento('3', 'Elver Galarga', '2016-04-08T17:45:00', '2016-04-08T18:00:00', 'orange');
            crearEvento('4', 'Benito Camelo', '2016-04-08T18:00:00', '2016-04-08T18:15:00', 'orange');
            crearEvento('5', 'Ledesma Madas', '2016-04-08T18:15:00', '2016-04-08T18:30:00', 'orange');
        </script>        
        
        <div id="dialog-cita" title="Crear cita médica">
            Seleccionar paciente
            <form>
            <br>
            <select id="selectNombre">
                <option>Cristhian Carreño</option>
                <option>Rosa Melano</option>
                <option>Elver Galarga</option>
                <option>Elver Galinda</option>
                <option>Benito Camelo</option>
                <option>Ledesma Madas</option>
            </select><br>
            
            <div id="divDialogMessage"></div>
            <br>

            </form>
        </div>        
        
        <div id="dialog-delete" title="Eliminar cita médica">
            <form>            
            <br>    
            Está seguro que desea eliminar la reserva.
            <br>
            <div id="divDeleteMsg"></div>
            <br>
            </form>
        </div>
        
</body>
</html>