
var arregloEventos = [];

function crearAgenda(divCalendar, fechaHoraActual, divDialog, divDialogMessage){
    	                
$('#'+divCalendar).fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
                                //right: 'month,basicWeek,basicDay'
			},
			defaultDate: $.now(),
                        defaultView: 'basicDay', // month,basicWeek,basicDay,agendaWeek,agendaDay
                        weekends: true, // mostrar fines de semana
                        hiddenDays: [ 0 ], // esconder días
                        //fixedWeekCount: false, // muestra el número de semanas que se pueden visualizar
                        //weekNumbers: true, // mostrar los números de las semanas
                        //handleWindowResize: true, // redimensionar automáticamente
                        timezone: 'America/Guayaquil',
                        lang: 'es',
                        selectable: true,
			selectHelper: true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
                        allDaySlot: false, // mostrar la pestaña todos los días
                        slotLabelFormat: 'H(:mm)a', //
                        slotDuration: '00:15:00', //tiempo de duracción de cada slot
                        slotEventOverlap: false, //indica si se puede o no sobresolapar los tiempos 
                        minTime: '08:00:00',
                        maxTime: '20:00:00',
                        nowIndicator: true,
                        /*dayRender: function(date, cell){ // deshabilitar días anteriores a la fecha
                            var moment = $('#'+divCalendar).fullCalendar('getDate');
                            //if (date > moment.format()){
                                $(cell).addClass('disabled');
                            //}
                        },*/
                        /*eventRender: function(event, element) {
                            element.bind('dblclick', function() {
                               alert('double click!');
                            });
                        },*/                        
                        eventClick: function(calEvent, jsEvent, view) {
                            alert('Event: ' + calEvent.title);
                            //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                            alert('View: ' + view.name);
                            // change the border color just for fun
                            $(this).css('border-color', 'red');
                        },
                        //dayClick: function() { alert('a day has been clicked!');},// acción en el click en el día
                        //Modal Event Title
                        
                        select: function(start, end) {

                            //alert(start.format() +'///'+fechaHoraActual.format());
                            
                               if (start.format() > fechaHoraActual.format()){
                                  
                                $("#"+divDialog).dialog();
                                $("#"+divDialogMessage).html('<br>Agregar paciente <hr> Inicio cita: ' + start.format() + ' <br>Fin cita: ' + end.format());
                                
                                //var title = prompt('Agregar paciente'); //\n\nInicio cita: ' + start.format() + ' \nFin cita: ' + fechaHoraActual.format());
				//var eventData;
                                
				//if (title) {
					/*eventData = {
						//title: title,
						start: start,
						end: end,
                                                color: 'orange'
					};*/
                                        
                        $('#dateStart').val(start);
                        $('#dateEnd').val(end);
                                        
			//$('#calendar').fullCalendar('renderEvent', eventData, true); // Crea el evento en el panel
                                        
				//}
                                
			//$('#calendar').fullCalendar('unselect'); // actualiza inmediatamente los campos
                                    
                                }else{
                                    alert('No se puede dar turno para esa fecha seleccionada.');
                                }
			},
			
		});		
}

function displayEvento(idPaciente, nombrePaciente, inicio, fin, color){
    var makeEvent = {
      id: idPaciente,
      title: nombrePaciente,
      start: inicio,
      end: fin,
      color: color
    };
    
    $('#calendar').fullCalendar( 'renderEvent', makeEvent, true );
}

function crearEvento(idPaciente, nombrePaciente, inicio, fin, color){
    var makeEvent = {
      id: idPaciente,
      title: nombrePaciente,
      start: inicio,
      end: fin,
      color: color
    };
    
   //arregloEventos.push(makeEvent);
    
    //$.each(arregloEventos, function (index, value) {

      $('#calendar').fullCalendar( 'renderEvent', makeEvent, true );
      
    //});
    
    
}

